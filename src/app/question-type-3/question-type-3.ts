import {Component, HostListener, ViewChild, Output, EventEmitter} from '@angular/core';
import {MathInputField} from '../directives/math-input-field/math-input-field';

@Component({
    selector: 'question-type3',
    styles: [`    
        .toolbar {
            background-color: #eee; 
        }
        .mq-editable-field {
            display: inline-block;
            width: 100%;
            font-size: 1.5em;
        }
        .keyboard-col {
            display: inline-block;
            vertical-align: text-top;
        }
        .keyboard-grid-row {
            padding: 5px;
        }
        .keyboard-item {
            display: inline-block;
            vertical-align: text-top;
            min-width: 30px;
            min-height: 30px;
            border: 1px solid;
            font-size: 1.2em;
            text-align: center;
        } 
        .question-type-3 {
            padding: 5px;
        }   
    `],
    directives: [MathInputField],
    templateUrl:  require('./question-type-3.html') 
})
export class QuestionType3 {
  currentAnswer: string
  answerHistory: Array<string>
  undoneAnswers: Array<string>
  wasUndoOrRedo: boolean
  KeyboardFracImg: string
  KeyboardPowerImg: string
  @ViewChild(MathInputField)
  private _mathInputField:MathInputField;
  @Output() isCorrect: EventEmitter<boolean> = new EventEmitter();

  constructor() {
      this.answerHistory = [];
      this.undoneAnswers = [];
      this.currentAnswer="";
      this.KeyboardFracImg = require('./KeyboardFrac.png');
      this.KeyboardPowerImg = require('./KeyboardPower.png')
  }
  
  checkAnswer() {
        // Sanitize Input
        var myAnswer = this.currentAnswer.replace('\\right','').replace('\\left','').replace('\\times','\\cdot');
        var validAnswers = ['((6+15)\\cdot3)+\\frac{2}{5}',
                            '(6+15)\\cdot3+\\frac{2}{5}',
                            '(((6+15)\\cdot3)+\\frac{2}{5})',
                            '((6+15)\\cdot3+\\frac{2}{5})',
                            '((6+15)\\cdot3+(\\frac{2}{5}))',
                            '(6+15)\\cdot3+(\\frac{2}{5})',
                            '((6+15)\\cdot3)+(\\frac{2}{5})',
                            '3\\cdot(6+15)+\\frac{2}{5}'];
                            
        if(validAnswers.indexOf(myAnswer) > -1) {
            console.log('Answer is correct');
            this.isCorrect.emit(true);
        }
        else
        {
            console.log('Answer is in-correct' + myAnswer );
            this.isCorrect.emit(false);
        }
  }
  onAnswerChanged(newAnswer) {
      if((!this.wasUndoOrRedo) && (this.currentAnswer != newAnswer)) {
          this.answerHistory.push(this.currentAnswer);
          this.undoneAnswers = [];
      }
      this.wasUndoOrRedo = false;
      this.currentAnswer = newAnswer;
      this.checkAnswer();
      console.log('Got Answer Changed Event ' + newAnswer);
  }
  onMoveLeft() {
      this._mathInputField.MoveLeft();
  }
  onMoveRight() {
      this._mathInputField.MoveRight();
  }
  onBackspace() {
      if(this.currentAnswer.length > 0) {
          this._mathInputField.SetNewAnswer(this.currentAnswer.slice(0, -1));
      }
  }
  onUndoClick() {
      if(this.answerHistory.length == 0) {
          return;
      }
            
      this.undoneAnswers.push(this.currentAnswer);
      this.currentAnswer = this.answerHistory.pop();
      this.wasUndoOrRedo=true;
      this._mathInputField.SetNewAnswer(this.currentAnswer);
      
  }
  onRedoClick() {
      if(this.undoneAnswers.length ==  0) {
          return;
      }
      this.wasUndoOrRedo = true;
      this.answerHistory.push(this.currentAnswer)
      this.currentAnswer = this.undoneAnswers.pop();
      this._mathInputField.SetNewAnswer(this.currentAnswer);     
  }
  onKeyClick(evt, key) {
        
        console.log('Keypress was '+ key);
        this._mathInputField.UpdateFromKeyPress(key);
    }
}