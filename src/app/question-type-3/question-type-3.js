System.register(['@angular/core', '../directives/math-input-field/math-input-field'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, math_input_field_1;
    var QuestionType3;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (math_input_field_1_1) {
                math_input_field_1 = math_input_field_1_1;
            }],
        execute: function() {
            QuestionType3 = (function () {
                function QuestionType3() {
                    this.isCorrect = new core_1.EventEmitter();
                    this.answerHistory = [];
                    this.undoneAnswers = [];
                    this.currentAnswer = "";
                }
                QuestionType3.prototype.checkAnswer = function () {
                    // Sanitize Input
                    var myAnswer = this.currentAnswer.replace('\\right', '').replace('\\left', '');
                    var validAnswers = ['((6+15)\\cdot3)+\\frac{2}{5}',
                        '(6+15)\\cdot3+\\frac{2}{5}',
                        '(((6+15)\\cdot3)+\\frac{2}{5})',
                        '((6+15)\\cdot3+\\frac{2}{5})',
                        '((6+15)\\cdot3+(\\frac{2}{5}))',
                        '(6+15)\\cdot3+(\\frac{2}{5})',
                        '((6+15)\\cdot3)+(\\frac{2}{5})'];
                    if (validAnswers.indexOf(myAnswer) > -1) {
                        this.isCorrect.emit(true);
                    }
                    else {
                        this.isCorrect.emit(false);
                    }
                };
                QuestionType3.prototype.onAnswerChanged = function (newAnswer) {
                    if ((!this.wasUndoOrRedo) && (this.currentAnswer != newAnswer)) {
                        this.answerHistory.push(this.currentAnswer);
                        this.undoneAnswers = [];
                    }
                    this.wasUndoOrRedo = false;
                    this.currentAnswer = newAnswer;
                    this.checkAnswer();
                    console.log('Got Answer Changed Event ' + newAnswer);
                };
                QuestionType3.prototype.onMoveLeft = function () {
                    this._mathInputField.MoveLeft();
                };
                QuestionType3.prototype.onMoveRight = function () {
                    this._mathInputField.MoveRight();
                };
                QuestionType3.prototype.onBackspace = function () {
                    if (this.currentAnswer.length > 0) {
                        this._mathInputField.SetNewAnswer(this.currentAnswer.slice(0, -1));
                    }
                };
                QuestionType3.prototype.onUndoClick = function () {
                    if (this.answerHistory.length == 0) {
                        return;
                    }
                    this.undoneAnswers.push(this.currentAnswer);
                    this.currentAnswer = this.answerHistory.pop();
                    this.wasUndoOrRedo = true;
                    this._mathInputField.SetNewAnswer(this.currentAnswer);
                };
                QuestionType3.prototype.onRedoClick = function () {
                    if (this.undoneAnswers.length == 0) {
                        return;
                    }
                    this.wasUndoOrRedo = true;
                    this.answerHistory.push(this.currentAnswer);
                    this.currentAnswer = this.undoneAnswers.pop();
                    this._mathInputField.SetNewAnswer(this.currentAnswer);
                };
                QuestionType3.prototype.onKeyClick = function (evt, key) {
                    console.log('Keypress was ' + key);
                    this._mathInputField.UpdateFromKeyPress(key);
                };
                __decorate([
                    core_1.ViewChild(math_input_field_1.MathInputField), 
                    __metadata('design:type', math_input_field_1.MathInputField)
                ], QuestionType3.prototype, "_mathInputField", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], QuestionType3.prototype, "isCorrect", void 0);
                QuestionType3 = __decorate([
                    core_1.Component({
                        selector: 'question-type3',
                        styles: ["    \n        .toolbar {\n            background-color: #eee; \n        }\n        .mq-editable-field {\n            display: inline-block;\n            width: 100%;\n            font-size: 1.5em;\n        }\n        .keyboard-col {\n            display: inline-block;\n            vertical-align: text-top;\n        }\n        .keyboard-grid-row {\n            padding: 5px;\n        }\n        .keyboard-item {\n            display: inline-block;\n            vertical-align: text-top;\n            min-width: 30px;\n            min-height: 30px;\n            border: 1px solid;\n            font-size: 1.2em;\n            text-align: center;\n        } \n        .question-type-3 {\n            padding: 5px;\n        }   \n    "],
                        directives: [math_input_field_1.MathInputField],
                        templateUrl: 'app/question-type-3/question-type-3.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], QuestionType3);
                return QuestionType3;
            }());
            exports_1("QuestionType3", QuestionType3);
        }
    }
});
//# sourceMappingURL=question-type-3.js.map