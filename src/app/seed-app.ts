import {Component} from '@angular/core';
import {Router, RouteConfig, ROUTER_DIRECTIVES} from '@angular/router-deprecated';

import {Home} from './components/home/home';
import {About} from './components/about/about';
import {RepoBrowser} from './components/repo-browser/repo-browser';
import {SampleApp} from './sample-questions/sample-questions';

require('../bower_components/bootstrap/dist/css/bootstrap.min.css');
require('../bower_components/font-awesome/css/font-awesome.css');
require('../styles.css');
require('file?name=fonts/[name].[ext]!../bower_components/font-awesome/fonts/fontawesome-webfont.woff2')
require('file?name=css/font/[name].[ext]!../ext-libs/mathquill/font/Symbola.ttf')
require('file?name=css/font/[name].[ext]!../ext-libs/mathquill/font/Symbola.woff')
require('file?name=css/font/[name].[ext]!../ext-libs/mathquill/font/Symbola.woff2')
require('file?name=css/font/[name].[ext]!../ext-libs/mathquill/font/Symbola.otf')

require('../ext-libs/mathquill/mathquill.css');

@Component({
  selector: 'seed-app',
  providers: [],
  pipes: [],
  directives: [ROUTER_DIRECTIVES],
  templateUrl: require('./seed-app.html'),
})
@RouteConfig([
  { path: '/...',       component: SampleApp,        name: 'Home', useAsDefault: true }
])
export class SeedApp {

  constructor() {}

}
