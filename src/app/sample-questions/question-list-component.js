System.register(['@angular/core', '@angular/router-deprecated', '../question-type-1/question-type-1', '../question-type-2/question-type-2', '../question-type-3/question-type-3', '../question-type-4/question-type-4'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, router_deprecated_1, question_type_1_1, question_type_2_1, question_type_3_1, question_type_4_1;
    var QuestionList;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (router_deprecated_1_1) {
                router_deprecated_1 = router_deprecated_1_1;
            },
            function (question_type_1_1_1) {
                question_type_1_1 = question_type_1_1_1;
            },
            function (question_type_2_1_1) {
                question_type_2_1 = question_type_2_1_1;
            },
            function (question_type_3_1_1) {
                question_type_3_1 = question_type_3_1_1;
            },
            function (question_type_4_1_1) {
                question_type_4_1 = question_type_4_1_1;
            }],
        execute: function() {
            QuestionList = (function () {
                function QuestionList() {
                    this.curQuestion = 1;
                    this.isCorrectQuestions = [];
                    this.isCorrectQuestions[0] = false;
                    this.isCorrectQuestions[1] = false;
                    this.isCorrectQuestions[2] = false;
                    this.isCorrectQuestions[3] = false;
                    this.isCorrectQuestions[4] = false;
                }
                QuestionList.prototype.onQuestionAnswerChange = function (question, isCorrect) {
                    this.isCorrectQuestions[question] = isCorrect;
                };
                QuestionList.prototype.nextQuestion = function () {
                    $("#flipbook").turn("next");
                    this.curQuestion += 1;
                };
                QuestionList.prototype.prevQuestion = function () {
                    if (this.curQuestion == 1) {
                        return;
                    }
                    $("#flipbook").turn("previous");
                    this.curQuestion -= 1;
                };
                QuestionList.prototype.startOver = function () {
                    location.reload();
                };
                QuestionList.prototype.ngAfterContentInit = function () {
                    $("#flipbook").turn({
                        width: 1280,
                        height: 580,
                        autoCenter: true
                    });
                };
                QuestionList = __decorate([
                    router_deprecated_1.RouteConfig([
                        { path: '/question-type-1', name: 'QuestionType1', component: question_type_1_1.QuestionType1, useAsDefault: true },
                        { path: '/question-type-2', name: 'QuestionType2', component: question_type_2_1.QuestionType2 },
                        { path: '/question-type-3', name: 'QuestionType3', component: question_type_3_1.QuestionType3 },
                        { path: '/question-type-4', name: 'QuestionType4', component: question_type_4_1.QuestionType4 }
                    ]),
                    core_1.Component({
                        templateUrl: '/app/sample-questions/question-list-component.html',
                        styles: ["\n       .btn {\n         color: white;\n       }\n       .logo {\n         margin-right: 10px;\n       }\n       .navbar\n       {\n          background-color: #1a90bc;\n          padding-left: 30px;\n          text-align: left;\n          color: white;\n       }\n    "],
                        directives: [router_deprecated_1.ROUTER_DIRECTIVES, question_type_1_1.QuestionType1, question_type_2_1.QuestionType2, question_type_3_1.QuestionType3, question_type_4_1.QuestionType4]
                    }), 
                    __metadata('design:paramtypes', [])
                ], QuestionList);
                return QuestionList;
            }());
            exports_1("QuestionList", QuestionList);
        }
    }
});
//# sourceMappingURL=question-list-component.js.map