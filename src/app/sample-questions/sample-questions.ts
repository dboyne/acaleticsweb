import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import {LoginScreen} from './login-component'
import {QuestionList} from './question-list-component';



@RouteConfig([
  {path: '/login', name: 'LoginScreen', component: LoginScreen},
  { // Crisis Center child route
    path: '/question-list/...',
    name: 'QuestionList',
    component: QuestionList, 
    useAsDefault: true
  }
])
@Component({
    selector: 'sample-app',
    template: '<router-outlet></router-outlet>',
    directives: [ROUTER_DIRECTIVES]
})
export class SampleApp {
   
}