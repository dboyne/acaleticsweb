import {Component} from '@angular/core';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';
import {QuestionType1} from '../question-type-1/question-type-1';
import {QuestionType2} from '../question-type-2/question-type-2';
import {QuestionType3} from '../question-type-3/question-type-3';
import {QuestionType4} from '../question-type-4/question-type-4';

require('../../index.html');

@RouteConfig([
  {path: '/question-type-1', name: 'QuestionType1', component: QuestionType1, useAsDefault: true},
  {path: '/question-type-2', name: 'QuestionType2', component: QuestionType2},
  {path: '/question-type-3', name: 'QuestionType3', component: QuestionType3},
  {path: '/question-type-4', name: 'QuestionType4', component: QuestionType4}
])
@Component({    
    templateUrl: require('./question-list-component.html'),
    styles: [`
       .btn {
         color: white;
       }
       .logo {
         margin-right: 10px;
       }
       .navbar
       {
          background-color: #1a90bc;
          padding-left: 30px;
          text-align: left;
          color: white;
       }
    `],
    directives: [ROUTER_DIRECTIVES, QuestionType1, QuestionType2, QuestionType3, QuestionType4]
})
export class QuestionList {
   curQuestion: number;
   isCorrectQuestions: Array<boolean>;
   headerLogo: string;
   Logo: string;
   constructor() {
     this.curQuestion = 1;
     this.isCorrectQuestions = [];
     this.isCorrectQuestions[0] = false;
     this.isCorrectQuestions[1] = false;
     this.isCorrectQuestions[2] = false;
     this.isCorrectQuestions[3] = false;
     this.isCorrectQuestions[4] = false;
     this.Logo = require('./images/LogoFinal.png')
   }
   onQuestionAnswerChange(question: number, isCorrect:boolean) {
         this.isCorrectQuestions[question]= isCorrect;
   }
   nextQuestion() {
     (<any>$("#flipbook")).turn("next");
     this.curQuestion += 1;
   }
   prevQuestion() {
     if(this.curQuestion == 1) {
       return;
     }
     (<any>$("#flipbook")).turn("previous");
     this.curQuestion -= 1;
   }
   startOver() {
     location.reload();
   }
   ngAfterContentInit() {
         (<any>$("#flipbook")).turn({
            width: 1280,
            height: 580,
            autoCenter: true
          });
   }
}