import {Component} from '@angular/core';
import {ROUTER_DIRECTIVES } from '@angular/router';
@Component({
    templateUrl: 'app/sample-questions/login-component.html',
    selector: 'login-screen',
    directives: [ROUTER_DIRECTIVES]
})
export class LoginScreen {
   
}