import {bootstrap}    from '@angular/platform-browser-dynamic';
import {SampleApp} from './sample-questions/sample-questions';
import {RouteConfig, ROUTER_DIRECTIVES, ROUTER_PROVIDERS } from '@angular/router-deprecated';

bootstrap(SampleApp, [ROUTER_PROVIDERS]);
