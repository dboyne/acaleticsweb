import {Component, HostListener, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'question-type2',
    templateUrl:  require('./question-type-2.html')
})
export class QuestionType2 {
    totalBagsSelected: number;
    numberOfLineMarkers: any;
    backgroundImage: string;
    @Output() isCorrect: EventEmitter<boolean> = new EventEmitter();
  
    constructor() {
        this.numberOfLineMarkers = new Array(33);
        this.backgroundImage = require('./Q2Background.png')
    }
    
   checkAnswer() {
        if(this.totalBagsSelected == 16) {
            this.isCorrect.emit(true);
        }
        else
        {
            this.isCorrect.emit(false);
        }
    }
    onMarkerClick(evt) {
        if(evt.srcElement.style.opacity == '0')
        {
           evt.srcElement.style.opacity = '1'; 
        }
        else
        {
            evt.srcElement.style.opacity = '0';
        }       
        this.checkAnswer();
        
    }
    
    selectTotalBags(selBags) {
        this.totalBagsSelected = selBags;
        this.checkAnswer();
    }
}