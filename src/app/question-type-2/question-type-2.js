System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var QuestionType2;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            QuestionType2 = (function () {
                function QuestionType2() {
                    this.isCorrect = new core_1.EventEmitter();
                    this.numberOfLineMarkers = new Array(33);
                }
                QuestionType2.prototype.checkAnswer = function () {
                    if (this.totalBagsSelected == 16) {
                        this.isCorrect.emit(true);
                    }
                    else {
                        this.isCorrect.emit(false);
                    }
                };
                QuestionType2.prototype.onMarkerClick = function (evt) {
                    if (evt.toElement.style.opacity == '0') {
                        evt.toElement.style.opacity = '1';
                    }
                    else {
                        evt.toElement.style.opacity = '0';
                    }
                    this.checkAnswer();
                };
                QuestionType2.prototype.selectTotalBags = function (selBags) {
                    this.totalBagsSelected = selBags;
                    this.checkAnswer();
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], QuestionType2.prototype, "isCorrect", void 0);
                QuestionType2 = __decorate([
                    core_1.Component({
                        selector: 'question-type2',
                        templateUrl: 'app/question-type-2/question-type-2.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], QuestionType2);
                return QuestionType2;
            }());
            exports_1("QuestionType2", QuestionType2);
        }
    }
});
//# sourceMappingURL=question-type-2.js.map