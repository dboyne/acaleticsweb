System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var QuestionType1;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            QuestionType1 = (function () {
                function QuestionType1() {
                    this.isCorrect = new core_1.EventEmitter();
                    this.selectedGraphLine = null;
                    this.FractionXPos = 234;
                    this.FractionYPos = 337;
                    this.fractionSelected = false;
                    this.hasFractionMoved = false;
                    this.fractionDisplay = "none";
                    this.locationsAndSnapPoints = [
                        {
                            yPos: 102,
                            snapPosints: [[150, 0], [331, 0.5], [510, 1]]
                        },
                        {
                            yPos: 212,
                            snapPosints: [[150, 0], [270, 1 / 3], [390, 2 / 3], [510, 1]]
                        },
                        {
                            yPos: 322,
                            snapPosints: [[150, 0], [241, 1 / 4], [330, 2 / 4], [420, 3 / 4], [510, 1]]
                        },
                        {
                            yPos: 417,
                            snapPosints: [[327, 0]]
                        }
                    ];
                }
                QuestionType1.prototype.checkAnswer = function () {
                    if (this.FractionXPos == 300 && this.FractionYPos == 132) {
                        this.isCorrect.emit(true);
                    }
                    else {
                        this.isCorrect.emit(false);
                    }
                };
                QuestionType1.prototype.activateLineSel = function (evt, idx) {
                    this.selectedGraphLine = idx;
                };
                QuestionType1.prototype.findClosestVerticalLocation = function (y) {
                    var resultLocation = null;
                    var yDiff = 0;
                    for (var i = 0; i < this.locationsAndSnapPoints.length; i++) {
                        var curLocation = this.locationsAndSnapPoints[i];
                        var curYDiff = Math.abs(curLocation.yPos - y);
                        if (resultLocation == null || curYDiff < yDiff) {
                            yDiff = curYDiff;
                            resultLocation = curLocation;
                        }
                    }
                    return resultLocation;
                };
                QuestionType1.prototype.findClosestXLocation = function (yLocData, x) {
                    var result = null;
                    var xDiff = 0;
                    for (var i = 0; i < yLocData.snapPosints.length; i++) {
                        var curLocation = yLocData.snapPosints[i];
                        var curXDiff = Math.abs(curLocation[0] - x);
                        if (result == null || curXDiff < xDiff) {
                            xDiff = curXDiff;
                            result = {
                                yPos: yLocData.yPos,
                                xPos: curLocation[0],
                                value: curLocation[1]
                            };
                        }
                    }
                    return result;
                };
                QuestionType1.prototype.findClosestSnapPointLocation = function (x, y) {
                    var closestYLoc = this.findClosestVerticalLocation(y);
                    if (closestYLoc == null) {
                        return;
                    }
                    var result = this.findClosestXLocation(closestYLoc, x);
                    return result;
                };
                QuestionType1.prototype.getTransformValue = function () {
                    return "translate(" + this.FractionXPos + "," + this.FractionYPos + ")";
                };
                QuestionType1.prototype.checkBoundary = function (evt) {
                    if ((evt.offsetX < 100) || (evt.offsetX > 560)) {
                        return false;
                    }
                    if ((evt.offsetY < 85) || (evt.offsetY > 420)) {
                        return false;
                    }
                    return true;
                };
                QuestionType1.prototype.onMoveFraction = function (evt) {
                    if (this.fractionSelected && this.checkBoundary(evt)) {
                        this.FractionXPos = evt.offsetX - 90;
                        this.FractionYPos = evt.offsetY - 80;
                        this.hasFractionMoved = true;
                    }
                };
                QuestionType1.prototype.snapFractionToClosestLoc = function (curX, curY) {
                    var closestLocData = this.findClosestSnapPointLocation(curX, curY);
                    this.FractionXPos = closestLocData.xPos - 90;
                    this.FractionYPos = closestLocData.yPos - 80;
                };
                QuestionType1.prototype.onFractionMouseUp = function (e) {
                    if (this.fractionSelected && this.hasFractionMoved) {
                        this.fractionSelected = false;
                        this.fractionDisplay = "none";
                        this.snapFractionToClosestLoc(e.offsetX, e.offsetY);
                        this.checkAnswer();
                    }
                    this.hasFractionMoved = false;
                };
                QuestionType1.prototype.onFractionMouseDown = function (e) {
                    if (!this.fractionSelected) {
                        this.fractionDisplay = "";
                        this.fractionSelected = true;
                    }
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], QuestionType1.prototype, "isCorrect", void 0);
                __decorate([
                    core_1.HostListener('mouseup', ['$event']), 
                    __metadata('design:type', Function), 
                    __metadata('design:paramtypes', [Object]), 
                    __metadata('design:returntype', void 0)
                ], QuestionType1.prototype, "onFractionMouseUp", null);
                QuestionType1 = __decorate([
                    core_1.Component({
                        selector: 'question-type1',
                        templateUrl: 'app/question-type-1/question-type-1.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], QuestionType1);
                return QuestionType1;
            }());
            exports_1("QuestionType1", QuestionType1);
        }
    }
});
//# sourceMappingURL=question-type-1.js.map