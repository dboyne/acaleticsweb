import {Component, HostListener, Output, EventEmitter} from '@angular/core';

var backgroundImg = require("./Background.png");
var targeFractionImg = require("./TargeFraction.png");

@Component({
    selector: 'question-type1',
    templateUrl: require('./question-type-1.html')
})
export class QuestionType1 { 
  fractionSelected: boolean;
  fractionDisplay: string;
  FractionXPos: number;
  FractionYPos: number;
  hasFractionMoved: boolean;
  locationsAndSnapPoints: any;
  selectedGraphLine: any;
  lastFracLoc: any;
  targeFractionImg: string;
  backgroundImg: string;
  @Output() isCorrect: EventEmitter<boolean> = new EventEmitter();
  
  checkAnswer() {
      console.log('Position x ' + this.FractionXPos + ', ' + this.FractionYPos);
      if(this.FractionXPos == 300 && this.FractionYPos == 134) {
          this.isCorrect.emit(true);
      }
      else
      {
          this.isCorrect.emit(false);
      }
  }
  activateLineSel(evt: any, idx:number) {
      this.selectedGraphLine = idx;
  }
  findClosestVerticalLocation(y:number) {
      var resultLocation:any = null;
      var yDiff:number = 0;
      for (var i = 0; i < this.locationsAndSnapPoints.length; i++) { 
            var curLocation  = this.locationsAndSnapPoints[i];
            var curYDiff = Math.abs(curLocation.yPos - y);
            if(resultLocation == null || curYDiff < yDiff) {
                yDiff = curYDiff;
                resultLocation = curLocation;
            }
        }
     return resultLocation;
  }
  
  findClosestXLocation(yLocData:any, x:number) {
      var result:any = null;
      var xDiff:number = 0;
      
      for (var i = 0; i < yLocData.snapPosints.length; i++) { 
            var curLocation  = yLocData.snapPosints[i];
            var curXDiff = Math.abs(curLocation[0] - x);
            if(result == null || curXDiff < xDiff) {
                xDiff = curXDiff;
                result = {
                    yPos: yLocData.yPos,
                    xPos: curLocation[0],
                    value: curLocation[1]
                }
            }
        }
     return result;
  }
  
  findClosestSnapPointLocation (x:number, y:number) {
      var closestYLoc = this.findClosestVerticalLocation(y);
      if(closestYLoc == null) {
          return;
      }
      
      var result = this.findClosestXLocation(closestYLoc, x);
      return result;
  }
  
  getTransformValue() {
      return "translate(" + this.FractionXPos + "," + this.FractionYPos + ")";
  }
  
  checkBoundary(evt) {
      if((evt.offsetX < 90) || (evt.offsetX > 580)) {
          return false;
      }
      if((evt.offsetY < 55) || (evt.offsetY > 405)) {
          return false;
      }
      return true;
  }
  onMoveFraction(evt) {
      
       if(this.fractionSelected && this.checkBoundary(evt)) {
           var dx = evt.offsetX - this.lastFracLoc.X;
           var dy = evt.offsetY - this.lastFracLoc.Y;

            this.FractionXPos =  this.FractionXPos +  dx;
            this.FractionYPos =  this.FractionYPos + dy;
            this.hasFractionMoved = true;
            this.lastFracLoc = {
                X: evt.offsetX,
                Y: evt.offsetY
            }
       }
  }
  
  snapFractionToClosestLoc(curX:number, curY:number) {
      var closestLocData = this.findClosestSnapPointLocation(curX, curY);
      this.FractionXPos = closestLocData.xPos;
      this.FractionYPos = closestLocData.yPos;
  }
  
  @HostListener('mouseup', ['$event'])
  onFractionMouseUp (e) {
        if(this.fractionSelected && this.hasFractionMoved) {
            this.fractionSelected = false;
            this.fractionDisplay = "none";
            this.snapFractionToClosestLoc(this.FractionXPos, this.FractionYPos);        
        }
        
        this.hasFractionMoved = false;
        this.checkAnswer();
  }
  
  onFractionMouseDown (e) {
        if(!this.fractionSelected) {
            this.fractionDisplay = "";
            this.fractionSelected = true;
            this.lastFracLoc = {
                X: e.offsetX,
                Y: e.offsetY
            }
        }           
        
    }
    
  constructor() {
    this.selectedGraphLine = null;
    this.FractionXPos = 240;
    this.FractionYPos = 337;
    this.fractionSelected = false;
    this.hasFractionMoved = false;
    this.fractionDisplay = "none";
    this.backgroundImg = require("./Background.png");
    this.targeFractionImg = require("./TargeFraction.png");
    
    this.locationsAndSnapPoints = [
        {
            yPos: 23, 
            snapPosints: [[60,0], [240,0.5], [420,1]]
        },
        {
            yPos: 134, 
            snapPosints: [[60,0], [180,1/3], [300,2/3], [420,1]]
        },
        {
            yPos: 243, 
            snapPosints: [[60,0], [151,1/4], [240,2/4], [330,3/4], [420,1]]
        },
        {
            yPos: 337, 
            snapPosints: [[240,0]]
        }
    ];
    
    
    
  }
}