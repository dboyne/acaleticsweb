System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var GraphWorkspace, DrawModeType;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            GraphWorkspace = (function () {
                function GraphWorkspace() {
                    this._canNotDraw = false;
                    this.onAnswerChange = new core_1.EventEmitter();
                    this.DrawMode = DrawModeType.None;
                    this.Points = [];
                    this.Lines = [];
                    this.MovingLine = null;
                    this.wasMouseDragged = false;
                }
                Object.defineProperty(GraphWorkspace.prototype, "canNotDraw", {
                    get: function () {
                        return this._canNotDraw;
                    },
                    set: function (newCanNotDraw) {
                        this._canNotDraw = newCanNotDraw;
                        this.CheckAnswer();
                    },
                    enumerable: true,
                    configurable: true
                });
                GraphWorkspace.prototype.CheckAnswer = function () {
                    var result = { isSquare: false, isRectangle: false, canNotDraw: false };
                    result.canNotDraw = this.canNotDraw;
                    // Check if lines are all connected    
                    if (this.Lines && (this.Lines.length == 4)) {
                        if (this.areLinesConnected()) {
                            if (this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[1]) ||
                                this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[2]) ||
                                this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[3])) {
                                result.isSquare = false;
                                result.isRectangle = true;
                            }
                            else {
                                result.isSquare = true;
                                result.isRectangle = true;
                            }
                        }
                    }
                    console.log(result);
                    this.onAnswerChange.emit(result);
                };
                GraphWorkspace.prototype.areLinesConnected = function () {
                    var result = false;
                    for (var i = 0; i < this.Lines.length; i++) {
                        var curLine = this.Lines[i];
                        var isConnected = false;
                        for (var j = 0; j < this.Lines.length; j++) {
                            var otherLine = this.Lines[j];
                            if (curLine != otherLine) {
                                isConnected = false;
                                if (this.isLineConnected(curLine, otherLine)) {
                                    isConnected = true;
                                    break;
                                }
                            }
                        }
                        if (!isConnected)
                            break;
                    }
                    return isConnected;
                };
                GraphWorkspace.prototype.isLineConnected = function (curLine, otherLine) {
                    if (!curLine || !otherLine) {
                        return false;
                    }
                    var result = (curLine.startX == otherLine.endX && curLine.startY == otherLine.endY) ||
                        (curLine.endX == otherLine.endX && curLine.endY == otherLine.endY) ||
                        (curLine.startX == otherLine.startX && curLine.startY == otherLine.startY);
                    return result;
                };
                GraphWorkspace.prototype.calcLineLength = function (curLine) {
                    var x1 = curLine.startX;
                    var y1 = curLine.startY;
                    var x2 = curLine.endX;
                    var y2 = curLine.endY;
                    if (!curLine) {
                        return 0;
                    }
                    return Math.sqrt((x1 -= x2) * x1 + (y1 -= y2) * y1);
                };
                GraphWorkspace.prototype.onMovMouse = function (evt) {
                    this.wasMouseDragged = true;
                    var xRounded = Math.round(evt.offsetX / 20);
                    var yRounded = Math.round(evt.offsetY / 20);
                    if (this.MovingLine != null) {
                        this.MovingLine.endX = xRounded * 20;
                        this.MovingLine.endY = yRounded * 20;
                    }
                };
                GraphWorkspace.prototype.onMouseUp = function (evt) {
                    if (this.wasMouseDragged) {
                        this.MovingLine = null;
                        this.wasMouseDragged = false;
                        this.CheckAnswer();
                    }
                };
                GraphWorkspace.prototype.onKeyPress = function (evt) {
                    console.log('key press');
                    if ((this.DrawMode != DrawModeType.Line) || (this.MovingLine == null)) {
                        return;
                    }
                    this.MovingLine = null;
                    this.Lines.pop();
                    this.CheckAnswer();
                };
                GraphWorkspace.prototype.onMouseDown = function (evt) {
                    this.wasMouseDragged = false;
                    //debugger;
                    if (!evt || !evt.offsetX || evt.offsetX < 1 || evt.offsetY < 1) {
                        return;
                    }
                    console.log('MouseDown detected at (' + evt.offsetX + "," + evt.offsetY + ")");
                    var xRounded = Math.round(evt.offsetX / 20);
                    var yRounded = Math.round(evt.offsetY / 20);
                    if (this.DrawMode == DrawModeType.Point) {
                        this.Points.push({
                            x: xRounded * 20,
                            y: yRounded * 20
                        });
                        this.MovingLine = null;
                    }
                    if (this.DrawMode == DrawModeType.Line) {
                        if (this.MovingLine == null) {
                            this.MovingLine = {
                                startX: xRounded * 20,
                                startY: yRounded * 20,
                                endX: xRounded * 20,
                                endY: yRounded * 20
                            };
                            this.Lines.push(this.MovingLine);
                        }
                        else {
                            this.MovingLine = null;
                        }
                    }
                    if (this.DrawMode == DrawModeType.Delete) {
                        this.MovingLine = null;
                        if (!evt || !evt.target || !evt.target.attributes["data-index"] || !evt.target.attributes["data-node-type"]) {
                            return;
                        }
                        if (evt.target.attributes["data-node-type"].value == "line") {
                            var lineIndex = evt.target.attributes["data-index"].value;
                            if (lineIndex) {
                                this.Lines.splice(lineIndex, 1);
                            }
                        }
                        else {
                            var pointIndex = evt.target.attributes["data-index"].value;
                            this.Points.splice(pointIndex, 1);
                        }
                    }
                    this.CheckAnswer();
                };
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], GraphWorkspace.prototype, "title", void 0);
                __decorate([
                    core_1.Input('draw-mode'), 
                    __metadata('design:type', Number)
                ], GraphWorkspace.prototype, "DrawMode", void 0);
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], GraphWorkspace.prototype, "onAnswerChange", void 0);
                GraphWorkspace = __decorate([
                    core_1.Component({
                        selector: 'graph-workspace',
                        styles: ["\n            .canNotDraw {\n                display: inline-block;\n                padding: 5px;\n                vertical-align: text-bottom;\n            }\n            .drawingSurface {\n                display: inline-block;\n            }\n            .graph-workspace {\n                width: 490px;\n                height: 220px;\n                border: 1px solid;\n                padding: 15px;\n                margin: 10px;\n            }\n            .canNotDrawCheckbox {\n                margin: 5px;\n            }\n            .title {\n                padding: 5px;\n            }\n    "],
                        templateUrl: 'app/components/graph-workspace/graph-workspace.html'
                    }), 
                    __metadata('design:paramtypes', [])
                ], GraphWorkspace);
                return GraphWorkspace;
            }());
            exports_1("GraphWorkspace", GraphWorkspace);
            (function (DrawModeType) {
                DrawModeType[DrawModeType["None"] = 0] = "None";
                DrawModeType[DrawModeType["Point"] = 1] = "Point";
                DrawModeType[DrawModeType["Line"] = 2] = "Line";
                DrawModeType[DrawModeType["Delete"] = 3] = "Delete";
            })(DrawModeType || (DrawModeType = {}));
            exports_1("DrawModeType", DrawModeType);
        }
    }
});
//# sourceMappingURL=graph-workspace.js.map