import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
    selector: 'graph-workspace',
    styles: [`
            .canNotDraw {
                display: inline-block;
                padding: 5px;
                vertical-align: text-bottom;
            }
            .drawingSurface {
                display: inline-block;
            }
            .graph-workspace {
                width: 490px;
                height: 220px;
                border: 1px solid;
                padding: 15px;
                margin: 10px;
            }
            .canNotDrawCheckbox {
                margin: 5px;
            }
            .title {
                padding: 5px;
            }
    `],
    templateUrl: require('./graph-workspace.html')
})
export class GraphWorkspace {
    @Input() title: string;
    private _canNotDraw: boolean = false;
    get canNotDraw():boolean {
        return this._canNotDraw;
    }
    set canNotDraw(newCanNotDraw:boolean) {
        this._canNotDraw = newCanNotDraw;
        this.CheckAnswer();
    }
    @Input('draw-mode') DrawMode: DrawModeType;
    private wasMouseDragged: boolean;
    Points: Array<any>;
    Lines: Array<any>;
    MovingLine: any;
    DrawingSurfaceImg: string;
    @Output() onAnswerChange: EventEmitter<any> = new EventEmitter();
    
    constructor() {
        this.DrawMode = DrawModeType.None;
        this.Points = [];
        this.Lines = [];
        this.MovingLine = null
        this.wasMouseDragged = false;
        this.DrawingSurfaceImg = require('./drawing-surface.png');
    }
    CheckAnswer() {
        var result = {isSquare: false, isRectangle: false, canNotDraw: false};
        
        result.canNotDraw = this.canNotDraw;
        
        // Check if lines are all connected    
        if(this.Lines && (this.Lines.length == 4)) {
           if(this.areLinesConnected()) { 
                if(this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[1]) ||
                    this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[2]) ||
                    this.calcLineLength(this.Lines[0]) != this.calcLineLength(this.Lines[3]))               
                    {
                        result.isSquare = false;
                        result.isRectangle= true; 
                    }
                    else {
                        result.isSquare = true;
                        result.isRectangle = true;
                    } 
            }
        }
        console.log(result);
        this.onAnswerChange.emit(result);
    }
    
    areLinesConnected() {
        var result = false;
        for(var i=0; i < this.Lines.length;i++ ) {
                    var curLine = this.Lines[i];
                    var isConnected = false;
                    for(var j=0; j < this.Lines.length;j++ ) {
                        var otherLine = this.Lines[j];
                        if(curLine != otherLine) {
                            isConnected = false;
                            if(this.isLineConnected(curLine, otherLine)) {
                                isConnected = true;
                                break;
                            }
                        }
                    }
                    if(!isConnected) break;
         }
         return isConnected;  
    }
    
    isLineConnected(curLine:any, otherLine:any) {
        if(!curLine || !otherLine) {
            return false;
        }
        var result = (curLine.startX == otherLine.endX &&  curLine.startY == otherLine.endY) ||
                        (curLine.endX == otherLine.endX &&  curLine.endY == otherLine.endY) ||
                        (curLine.startX == otherLine.startX &&  curLine.startY == otherLine.startY) ;
        return result;
    }
    calcLineLength(curLine: any) {
        var x1 = curLine.startX;
        var y1 = curLine.startY;
        var x2 = curLine.endX;
        var y2 = curLine.endY
        if(!curLine) {
            return 0;
        }
        return Math.sqrt((x1 -= x2) * x1 + (y1 -= y2) * y1);
    }
    onMovMouse(evt) {
       this.wasMouseDragged = true;
       var xRounded = Math.round(evt.offsetX / 20);
       var yRounded = Math.round(evt.offsetY / 20);
       if(this.MovingLine != null) {
            this.MovingLine.endX = xRounded * 20;
            this.MovingLine.endY = yRounded * 20;         
       }
    }
    onMouseUp(evt) {
        if(this.wasMouseDragged) {
            this.MovingLine = null;
            this.wasMouseDragged = false;
            this.CheckAnswer();
        }
    }
    
    onKeyPress(evt) {
        console.log('key press');
        if((this.DrawMode != DrawModeType.Line ) || (this.MovingLine == null))
        {
            return;
        }
        this.MovingLine = null;
        this.Lines.pop();
        this.CheckAnswer();
    }
    onMouseDown(evt) {
        this.wasMouseDragged = false;
        //debugger;
        if(!evt || !evt.offsetX || evt.offsetX < 1 || evt.offsetY < 1) {
            return;
        }
        console.log('MouseDown detected at (' + evt.offsetX + "," + evt.offsetY + ")");
        var xRounded = Math.round(evt.offsetX / 20);
        var yRounded = Math.round(evt.offsetY / 20);
        if(this.DrawMode == DrawModeType.Point) 
        {
            this.Points.push({
                x: xRounded * 20,
                y: yRounded * 20
            });
            this.MovingLine = null;
        } 

        if(this.DrawMode == DrawModeType.Line ) 
        {
            if(this.MovingLine  == null)
            {
                this.MovingLine = {
                    startX: xRounded * 20,
                    startY: yRounded * 20,
                    endX: xRounded * 20,
                    endY: yRounded * 20
                };
                this.Lines.push(this.MovingLine);
            }
            else
            {
                this.MovingLine  = null;
            }
        }
        
        if(this.DrawMode == DrawModeType.Delete)
        {
            this.MovingLine = null;
            if(!evt || !evt.target || !evt.target.attributes["data-index"] || !evt.target.attributes["data-node-type"]) {
                return;
            }
            
            if(evt.target.attributes["data-node-type"].value == "line") {
                var lineIndex = evt.target.attributes["data-index"].value;
                if(lineIndex) {
                    this.Lines.splice(lineIndex, 1);
                }               
            }
            else 
            {
                var pointIndex = evt.target.attributes["data-index"].value;
                this.Points.splice(pointIndex, 1);
            }
        }
        this.CheckAnswer();
    }
    
}

export enum DrawModeType {
    None,
    Point,
    Line,
    Delete
}