System.register(['@angular/core'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1;
    var MathInputField;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            MathInputField = (function () {
                function MathInputField(el) {
                    this.answerChanged = new core_1.EventEmitter();
                    this.curInputField = el.nativeElement;
                }
                MathInputField.prototype.setFocusOnField = function () {
                    this.answerObj.focus();
                };
                MathInputField.prototype.MoveLeft = function () {
                    if (this.answerObj) {
                        this.answerObj.keystroke('Left');
                        this.setFocusOnField();
                    }
                };
                MathInputField.prototype.MoveRight = function () {
                    if (this.answerObj) {
                        this.answerObj.keystroke('Right');
                        this.setFocusOnField();
                    }
                };
                MathInputField.prototype.UpdateFromKeyPress = function (key) {
                    if (this.answerObj) {
                        this.answerObj.cmd(key);
                        this.setFocusOnField();
                    }
                };
                MathInputField.prototype.SetNewAnswer = function (answer) {
                    if (this.answerObj) {
                        this.answerObj.latex(answer);
                        this.setFocusOnField();
                    }
                };
                MathInputField.prototype.ngAfterViewInit = function () {
                    var self = this;
                    var MQ = MathQuill.getInterface(2);
                    this.answerObj = MQ.MathField(this.curInputField, {
                        handlers: {
                            edit: function (evt) {
                                //debugger;
                                self.answerChanged.emit(self.answerObj.latex());
                            }
                        }
                    });
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', Object)
                ], MathInputField.prototype, "answerChanged", void 0);
                __decorate([
                    core_1.Input(), 
                    __metadata('design:type', String)
                ], MathInputField.prototype, "answer", void 0);
                MathInputField = __decorate([
                    core_1.Directive({
                        selector: 'math-input-field'
                    }), 
                    __metadata('design:paramtypes', [core_1.ElementRef])
                ], MathInputField);
                return MathInputField;
            }());
            exports_1("MathInputField", MathInputField);
        }
    }
});
//# sourceMappingURL=math-input-field.js.map