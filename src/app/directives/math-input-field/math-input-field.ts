import {Directive, ElementRef, Input, EventEmitter, Output} from '@angular/core';

@Directive({
    selector: 'math-input-field'
})
export class MathInputField {
    _answer:string;
    @Output() answerChanged = new EventEmitter<string>();
    @Input() answer:string;
    curInputField : any;
    answerObj: any;
    
    constructor(el: ElementRef) {
       this.curInputField = el.nativeElement;
    }
    
    setFocusOnField() {
        this.answerObj.focus();
    }
    MoveLeft() {
        if(this.answerObj) {
            this.answerObj.keystroke('Left');
            this.setFocusOnField();
        }
    }
    MoveRight() {
        if(this.answerObj) {
            this.answerObj.keystroke('Right');
            this.setFocusOnField();
        }
    }
    UpdateFromKeyPress(key:string) {
        if(this.answerObj) {
            this.answerObj.cmd(key);
            this.setFocusOnField();
        }
    }
    
    SetNewAnswer(answer:string) {
        if(this.answerObj) {
            this.answerObj.latex(answer);
            this.setFocusOnField()
        }
    }
    
     ngAfterViewInit()  {
        var self = this;
        var MQ = MathQuill.getInterface(2);
        this.answerObj = MQ.MathField(this.curInputField, {
            handlers: {
            edit: function(evt) {
                    //debugger;
                    self.answerChanged.emit(self.answerObj.latex());
                }
            }
        });
   }
}