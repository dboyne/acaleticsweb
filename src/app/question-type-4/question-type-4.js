System.register(['@angular/core', '../components/graph-workspace/graph-workspace'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, graph_workspace_1;
    var QuestionType4;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (graph_workspace_1_1) {
                graph_workspace_1 = graph_workspace_1_1;
            }],
        execute: function() {
            QuestionType4 = (function () {
                function QuestionType4() {
                    this.isCorrect = new core_1.EventEmitter();
                }
                QuestionType4.prototype.onDeleteMode = function () {
                    this.selectedMode = graph_workspace_1.DrawModeType.Delete;
                };
                QuestionType4.prototype.onPointMode = function () {
                    this.selectedMode = graph_workspace_1.DrawModeType.Point;
                };
                QuestionType4.prototype.onLineMode = function () {
                    this.selectedMode = graph_workspace_1.DrawModeType.Line;
                };
                QuestionType4.prototype.onQ1AnswerChange = function (curEvent) {
                    if (!curEvent.isSquare && curEvent.isRectangle && !curEvent.canNotDraw) {
                        this.isQ1Correct = true;
                    }
                    else {
                        this.isQ1Correct = false;
                    }
                    this.isCorrect.emit(this.isQ1Correct && this.isQ2Correct);
                };
                QuestionType4.prototype.onQ2AnswerChange = function (curEvent) {
                    if (curEvent.isSquare && curEvent.isRectangle && !curEvent.canNotDraw) {
                        this.isQ2Correct = true;
                    }
                    else {
                        this.isQ2Correct = false;
                    }
                    this.isCorrect.emit(this.isQ1Correct && this.isQ2Correct);
                };
                __decorate([
                    core_1.Output(), 
                    __metadata('design:type', core_1.EventEmitter)
                ], QuestionType4.prototype, "isCorrect", void 0);
                QuestionType4 = __decorate([
                    core_1.Component({
                        selector: 'question-type4',
                        templateUrl: 'app/question-type-4/question-type-4.html',
                        directives: [graph_workspace_1.GraphWorkspace]
                    }), 
                    __metadata('design:paramtypes', [])
                ], QuestionType4);
                return QuestionType4;
            }());
            exports_1("QuestionType4", QuestionType4);
        }
    }
});
//# sourceMappingURL=question-type-4.js.map