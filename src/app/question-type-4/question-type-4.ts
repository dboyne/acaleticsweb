import {Component, HostListener, ViewChild, Output, EventEmitter} from '@angular/core';
import {GraphWorkspace, DrawModeType} from '../components/graph-workspace/graph-workspace'

@Component({
    selector: 'question-type4',
    templateUrl: require('./question-type-4.html'),
    directives: [GraphWorkspace]
})
export class QuestionType4 {
    @Output() isCorrect: EventEmitter<boolean> = new EventEmitter();
    isQ1Correct: boolean;
    isQ2Correct: boolean;
    selectedMode: DrawModeType
    onDeleteMode() {
        this.selectedMode = DrawModeType.Delete
    }
    onPointMode() {
        this.selectedMode = DrawModeType.Point
    }
    onLineMode() {
        this.selectedMode = DrawModeType.Line
    }
    onQ1AnswerChange(curEvent) {
        if(!curEvent.isSquare &&  curEvent.isRectangle && !curEvent.canNotDraw) {
            this.isQ1Correct = true;
        } else {
            this.isQ1Correct = false;
        }
        this.isCorrect.emit(this.isQ1Correct && this.isQ2Correct );
    }
     onQ2AnswerChange(curEvent) {
        if(curEvent.isSquare &&  curEvent.isRectangle && !curEvent.canNotDraw) {
            this.isQ2Correct = true;
        } else {
            this.isQ2Correct = false;
        }
        this.isCorrect.emit(this.isQ1Correct && this.isQ2Correct);
    }
}